Demonstrates 3 different approaches for detecting the user's network connectivity.

1. Using Apple's provided Reachability class.
2. Using the well known Tony Million's Reachability alternative to Apple's solution.
3. Using a singleton manager to abstract the reachability details from client view controllers.