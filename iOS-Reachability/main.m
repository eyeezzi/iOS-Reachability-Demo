//
//  main.m
//  iOS-Reachability
//
//  Created by Uzziah ignatius Eyee on 2/29/16.
//  Copyright (c) 2016 Uzziah ignatius Eyee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
