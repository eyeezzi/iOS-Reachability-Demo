//
//  AppDelegate.h
//  iOS-Reachability
//
//  Created by Uzziah ignatius Eyee on 2/29/16.
//  Copyright (c) 2016 Uzziah ignatius Eyee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

